import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Lugar } from '../../interfaces/lugar';
import { HttpClient } from '@angular/common/http';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.scss']
})
export class MapaComponent implements OnInit {
  @ViewChild('map', {static: true}) mapaElement: ElementRef;
  map: google.maps.Map;
  marcadores: google.maps.Marker[] = [];
  infoWindows: google.maps.InfoWindow[] = [];

  lugares: Lugar[] = [];

  constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) { }

  ngOnInit(): void {
    this.http.get('http://localhost:5000/mapa')
    .subscribe((lugares: Lugar[]) => {
      this.lugares = [...lugares];
      this.cargarMapa();
    });

    this.escuharSockets();
  }

  escuharSockets() {
    // marcador-nuevo
    this.wsService.listen('marcador-nuevo')
    .subscribe((marcador: Lugar) => {
      this.agregarMarcador(marcador);
    });

    // marcador-mover
    this.wsService.listen('marcador-mover')
    .subscribe((marcador: Lugar) => {
      console.log(marcador);
      const latLng = new google.maps.LatLng(marcador.lat, marcador.lng);
      this.marcadores.find(m => m.getTitle() === marcador.title)
      .setPosition(latLng);
    });

    // marcador-borrar
    this.wsService.listen('marcador-borrar')
    .subscribe((id: string) => {
      this.lugares = this.lugares.filter(m => m.id !== id);
      this.marcadores.find(m => m.getTitle() === id).setMap(null);
    });

  }

  cargarMapa() {
    const latLng = new google.maps.LatLng(37.784679, -122.395936);

    this.map = new google.maps.Map(this.mapaElement.nativeElement, {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    this.map.addListener('click', (coors: any) => {
      const nuevoMarcador: Lugar = {
        nombre: 'Nuevo lugar',
        lat: coors.latLng.lat(),
        lng: coors.latLng.lng(),
        id: new Date().toISOString()
      };

      this.agregarMarcador(nuevoMarcador);

      this.wsService.emit('marcador-nuevo', nuevoMarcador);
    });

    for (const lugar of this.lugares) {
      this.agregarMarcador(lugar);
    }
  }

  agregarMarcador(marcador: Lugar) {
    const latLng = new google.maps.LatLng(marcador.lat, marcador.lng);
    const marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      draggable: true,
      title: marcador.id
    });
    this.marcadores = [...this.marcadores, marker];

    const contenido = `<b>${marcador.nombre}</b>`;
    const infoWindow = new google.maps.InfoWindow({
      content: contenido
    });
    this.infoWindows = [...this.infoWindows, infoWindow];

    google.maps.event.addDomListener(marker, 'click', (coors) => {
      this.infoWindows.forEach(infoW => infoW.close());
      infoWindow.open(this.map, marker);
    });

    google.maps.event.addDomListener(marker, 'dblclick', (coors) => {
      marker.setMap(null);
      this.wsService.emit('marcador-borrar', marker.getTitle());
    });

    google.maps.event.addDomListener(marker, 'drag', (coors: any) => {
      const nuevoMarcador = {
        lat: coors.latLng.lat(),
        lng: coors.latLng.lng(),
        nombre: marcador.nombre,
        title: marker.getTitle()
      };
      console.log(nuevoMarcador);
      this.wsService.emit('marcador-mover', nuevoMarcador);
    });

  }
}
