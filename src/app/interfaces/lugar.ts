export interface Lugar {
  id ?: string;
  title ?: string;
  nombre: string;
  lat: number;
  lng: number;
}
